'use strict';

angular.module('confusionApp')
    .constant('baseURL', 'http://localhost:3000/')

    .service('menuFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getDishes = function () {
            return $resource(baseURL + "dishes/:id", null, {'update': {method: 'PUT'}});
        };

        //// Week 4 Task 1 //////
        this.getPromotions = function () {
            return $resource(baseURL + "promotions/:id", null, {'update': {method: 'PUT'}});
        };

    }])

    .factory('corporateFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        var corpfac = {};

        //// Week 4 Task 2 //////
        corpfac.getLeaders = function () {
            return $resource(baseURL + "leadership/:id", null, {'update': {method: 'PUT'}});
        };

        return corpfac;
    }])

    //// Week 4 Task 3 //////
    .factory('feedbackFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        var feedfac = {};

        feedfac.getFeedback = function () {
            return $resource(baseURL + "feedback/:id", null, {'update': {method: 'PUT'}});
        };

        return feedfac;
    }])

;
