# Front-End JavaScript Frameworks: AngularJS

The Hong Kong University of Science and Technology

### Highlights
    
  - Angular JS Overview
  - Task Runners, Angular Scope, Forms and Form Validation
  - Angular Factory, Service and Dependency Injection
  - Client-Server Communication and Angular Testing

## Reference site

-  [Restaurant Details](https://confusion-02.firebaseapp.com/#/)
-  [Coursera HKU](https://www.coursera.org/learn/angular-js/home/info)


`Completed Dec 2016`