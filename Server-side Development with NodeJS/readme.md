## Server side development with Node Js


### Highlights

- Using Express Generators
- Basic Authentication
- OAuth
- Backend as a service using Loopback


`Completed by David Tan on July 20, 2016`